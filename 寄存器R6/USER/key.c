     //Filename: key.c
         #include "includes.h"
     
     void KEYInit(void)
     {
     	RCC->APB2ENR |= (1uL<<4);
			 
     	GPIOC->CRL &=~((3uL<<8)|(3uL<<12)|(3uL<<16));
			GPIOC->CRL |=(1uL<<10)|(1uL<<14)|(3uL<<18);
			GPIOC->CRL &=~((1uL<<11)|(1uL<<15)|(1uL<<19));
    	
    	
    }
    
		Int08U KEY_Scan(Int08U key)
		{
		switch(key)
		{
			case 0: return (GPIOC->IDR& GPIO_IDR_IDR4);
			case 1: return (GPIOC->IDR& GPIO_IDR_IDR3);
			case 2: return (GPIOC->IDR& GPIO_IDR_IDR2);
			default:return 1;
		}
		}