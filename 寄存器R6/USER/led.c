     //Filename: led.c
         #include "includes.h"
     
     void LEDInit(void)
     {
     	RCC->APB2ENR |= (1uL<<3) | (1uL<<2);
     	GPIOB->CRL |= (1uL<<20);
     	GPIOB->CRL &=~((1uL<<21) | (1uL<<22) | (1uL<<23));
    	
    	GPIOA->CRL |= (1uL<<20);
    	GPIOA->CRL &=~((1uL<<21) | (1uL<<22) | (1uL<<23));
    }
    
    void LED(Int08U w, LEDState s) 
    {
    	switch(w)
    	{
    		case 0:
    			if(s==LED_ON)
    			  GPIOB->BRR = (1uL<<5);
    			else
    			  GPIOB->BSRR = (1uL<<5);
    			break;
    		case 1:
    			if(s==LED_ON)
    			  GPIOA->BRR = (1uL<<5);	
    			else
    			  GPIOA->BSRR = (1uL<<5);
    			break;
    		default:
    			break;
    	}	
    }
